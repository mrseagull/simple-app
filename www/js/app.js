

var isAndroid = Framework7.prototype.device.android === true;
var isIos = Framework7.prototype.device.ios === true;


Template7.global = {
    android: isAndroid,
    ios: isIos
};


var $$ = Dom7;

if (isAndroid) {
	$$('.view.navbar-through').removeClass('navbar-through').addClass('navbar-fixed');
    $$('.view .navbar').prependTo('.view .page');

    $$('head').append('<link rel="stylesheet" href="css/framework7.material.min.css"><link rel="stylesheet" href="css/framework7.material.colors.min.css">');
}
if (isIos) {

    $$('head').append('<link rel="stylesheet" href="css/framework7.ios.min.css"><link rel="stylesheet" href="css/framework7.ios.colors.min.css">');
}


document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {

    document.addEventListener("backbutton", function () {
        if($$('.page-on-center .back')){
            mainView.router.back();
         } 
        
         else{
            myApp.confirm('', 'Выйти из приложения?', function () {
                navigator.app.exitApp();
            });
        }
    }, false);


    if(device.platform == 'ios'){
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
        
        StatusBar.styleDefault();
  
    window.addEventListener('native.keyboardshow', keyboardShowHandler);
    window.addEventListener('native.keyboardhide', keyboardHideHandler);

    function keyboardHideHandler(e) {
        console.log(keyboardHideHandler);
    }



    function keyboardShowHandler(e) {
         console.log('keyboardHeight '+ e.keyboardHeight); 
    }


    $$(document).on('focusin', 'textarea', function () {
        console.log('focusin', 'textarea');
        $$('.page-on-center .page-content').css('overflow', 'hidden');
    });

    $$(document).on('focusout', 'textarea', function () {
        console.log('focusout', 'textarea');
        $$('.page-on-center .page-content').css('overflow', 'scroll');
    });

    $$(document).on('touchstart mousedown focus', '.at_links', function () {
        if($$(this).find('input')){
            $$(this).find('input').click();
        }
    });
  
    }

    mainView.loadPage('listScreen.html');

}

var myApp = new Framework7({
    material: isAndroid ? true : false,
    template7Pages: true,
    modalButtonOk: 'Хорошо',
    modalButtonCancel: 'Отмена',
    modalPreloaderTitle: 'Загрузка',
      modalTitle:	'AppTor App',

});



$$('.item-menu-left').on('click', function () {
 myApp.closePanel();
});

var mainView = myApp.addView('.view-main', {
    dynamicNavbar: true
});

myApp.onPageInit('page', function (page) {

    $("#phone").mask("+7 (999) 999-99-99");

});

myApp.onPageInit('slider', function (page) {


var mySwiper2 = myApp.swiper('.swiper-2', {
  pagination:'.swiper-2 .swiper-pagination',
  spaceBetween: 10,
  slidesPerView: 3
});

});

