
function ajaxPOST(endPoint, dataString, successCallback, errorCallback, completeCallback) {
    console.info('POST request', dataString);
    myApp.showIndicator();
    $$.ajax({
        url: 'http://crm.apptor.ru/app/api.php' + endPoint,
        type: "POST",
        dataType: 'json',
        data: dataString,

        success: function (json) {
            myApp.hideIndicator();
            console.info('POST response', json);
            if (json.error) {
                if (errorCallback) errorCallback(json);
                //  myApp.alert(json.error.error_message);
            }
            else
                successCallback(json);
        },

        error: function (xhr, errmsg, err) {
            myApp.hideIndicator();
            console.error(xhr.status + ": " + xhr.responseText);
           
            if (errorCallback) errorCallback(xhr.responseText);
        },

        complete: function (xhr) {
            if (completeCallback) completeCallback(xhr.responseText);
        }
    });
}

function ajaxGET(endPoint, body, successCallback, errorCallback, completeCallback) {
    console.info('GET endPoint', endPoint, body);
    myApp.showIndicator();
    $$.ajax({
        url: 'http://crm.apptor.ru/app/api.php' + endPoint,
        type: "GET",
        dataType: 'json',
        data: body,

        success: function (json) {
            myApp.hideIndicator();
            console.info('GET response', json);
            try {
                if (json.error) {
                    if (errorCallback) errorCallback(json);
                }
                else
                    successCallback(json);
            }
            catch (e) {
                console.error(e);
            }

        },

        error: function (xhr, errmsg, err) {
            myApp.hideIndicator();
            console.error(xhr.status + ": " + xhr.responseText);
            if (errorCallback) errorCallback(xhr.responseText);
        },

        complete: function (xhr) {
            myApp.hideIndicator();
            if(xhr.responseText == 'Forbidden'){
                store = {};
                localStorage.clear();
                mainView.loadPage('login.html');
            } else if (completeCallback) completeCallback(xhr.responseText);
        }
    });

};

function lengthControl4(e){
    if (e.currentTarget.value.length > 4) {
        e.currentTarget.value = e.currentTarget.value.substring(0, 4);
    }
}

function lengthControl11(e){
    if (e.currentTarget.value.length > 11) {
        e.currentTarget.value = e.currentTarget.value.substring(0, 11);
    }
}

function showAlert(title,text){
    if(title){
        myApp.alert(text,title);
    }else{
        myApp.alert(text,'');
    }
}


function timeConverter(UNIX_timestamp){
	var a = new Date(UNIX_timestamp * 1000);
	var months = ['01','02','03','04','05','06','07','08','09','10','11','12'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	if(min<10){
		min='0'+min;
	}
	if(hour<10){
		hour='0'+hour;
	}
	var sec = a.getSeconds();
	var time =year+'-' + month+ '-' + date;
	return time;

}
